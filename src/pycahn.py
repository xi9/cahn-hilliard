#!/bin/python3
'''
A friendly application for the simulation and study of the cahn-hilliard
relation under various conditions using the spectral method.
'''
# By Matthew B. Wilson, August 25 2021


import sys
from typing import *
from pathlib import Path
import numpy as np
import parse_args


def pycahn(
    random_seed: int,
    widths: List[float],
    divisions: List[int],
    free_energy_str: str,
    free_energy_function: Callable[[np.ndarray], np.ndarray],
    iterations: int,
    time_step: int,
    gamma: Optional[float],
    use_temperature_function: bool,
    temperature_range: Optional[List[float]],
    a_values: Optional[List[float]],
    no_csv_output: bool,
    csv_output: bool,
    svg_output: bool,
    save_every: int,
    DIR: Path
):
    '''
    Application entry point
    '''
    DIR.mkdir()
    info_file_path = DIR / 'info.txt'
    with open(info_file_path, 'x') as info_file:
        lines = [
            'seed: {}'.format(str(random_seed)),
            'domain widths: {}'.format(str(widths)),
            'domain divisions: {}'.format(str(divisions)),
            'free energy function: g(c) = {}'.format(free_energy_str),
            'maximum iterations: {}'.format(str(iterations)),
            'time step: {}'.format(str(time_step)),
            'temperature function?: {}'.format(str(use_temperature_function)),
            'output csv: {}'.format(str(csv_output)),
            'output svg: {}'.format(str(svg_output)),
        ]
        lines = [line + '\n' for line in lines]
        info_file.writelines(lines)
    dom = SpectralDomain(widths, divisions)
    ch_sys = CahnHilliardSystem(a_values, free_energy_function)
    simulation = CahnHilliardSpectralSimulation(dom, )
    return 0


def main():
    args = parse_args.process_vargs()
    if args is not None:
        sys.exit(pycahn(**args))
    else:
        sys.exit(1)


if __name__ == "__main__":
    main()
